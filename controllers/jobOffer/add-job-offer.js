const { JobOffer } = require("../../models/jobOffer");

module.exports = {
  addJobOffer: async (req, res, next) => {
    try {
      // get request data
      const { title, description } = req.body;
      /* ------------------------------- Validation ------------------------------- */

      /* ----------------------------- Implementation ----------------------------- */

      const jobOffer = new JobOffer({
        recruiterId: "test",
        title,
        description,
      });

      jobOffer.save();
      const offers = await JobOffer.find();

      res.send(offers);
    } catch (error) {
      console.log("error", error);
      next(error);
    }
  },
};
