const UserService = require('../service/userService');


const userController2 = {

  getUserId: async (req, res) => {
    try {
      const user = await UserService.getUserById(req.params.id);

      if (user == null) {
        return res.status(404).json({ message: 'Cannot find user' });
      }

      res.json(user);
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  },
};




module.exports = userController2;
