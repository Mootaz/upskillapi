
const LikeService = require('../../service/Like/likeService');
const Like = require('../../models/course/Like');

async function like(req, res, next) {
    try {
      const { userId, videoId } = req.body;
  
      const savedLike = await LikeService.like(userId, videoId);
      res.status(200).json(savedLike);
    } catch (error) {
      console.error('Error liking:', error);
      res.status(500).json({ error: 'Failed to like' });
    }
  }

 


  async function getLikes(req, res, next) {
    const { videoId } = req.params;
  
    try {
      const likes = await Like.find({ Video_id: videoId });
      res.status(200).json(likes);
    } catch (error) {
      console.error('Error retrieving likes:', error);
      res.status(500).json({ error: 'Failed to retrieve likes' });
    }
  }
  
  
  
  module.exports = {
    like, getLikes
  };