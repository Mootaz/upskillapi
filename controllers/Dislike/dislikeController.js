
const DislikeService = require('../../service/Dislike/dislikeService');


  async function Dislike(req, res, next) {
    try {
      const { userId, videoId } = req.body;
  
      const savedLike = await DislikeService.Dislike(userId, videoId);
      res.status(200).json(savedLike);
    } catch (error) {
      console.error('Error liking:', error);
      res.status(500).json({ error: 'Failed to like' });
    }
  }



  
  
  module.exports = {
     Dislike
  };