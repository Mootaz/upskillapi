/* -------------------------------------------------------------------------- */
/*                                 Dependecies                                */
/* -------------------------------------------------------------------------- */
const { Message } = require("../../models/message");

/* -------------------------------------------------------------------------- */
/*                                Implementaion                               */
/* -------------------------------------------------------------------------- */
module.exports = {
  sendMessage: async (req, res, next) => {
    try {
      // get request data
      const { message } = req.body;

      /* ----------------------------- Implementation ----------------------------- */

      const messageObject = new Message({
        ...message,
        sentAt: new Date(),
      });

      messageObject.save();

      res.send(messageObject);
    } catch (error) {
      console.log("error", error);
      next(error);
    }
  },
};
