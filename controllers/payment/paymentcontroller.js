const PaymentService = require('../../service/payment/paymentservice');


async function createPayment(req, res) {
    try {
      const { email, amount, currency, source, cvc , date , User_id, paymentIntentId} = req.body;
      const payment = await PaymentService.createPayment(email, amount, currency, source, cvc, date , User_id, paymentIntentId);
        res.json({ payment });
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  }
  
  async function updatePaymentStatus(req, res) {
    try {
      const { paymentId } = req.params;
      const { status } = req.body;
      const payment = await PaymentService.updatePaymentStatus(paymentId, status);
      // replace 'https://example.com/payment' with your payment URL
      const paymentUrl = `http://localhost:4000/payment/${payment._id}`;
      res.json({ paymentUrl });
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  }


  async function getPaymentStatistics(req, res) {
    try {
      const statistics = await PaymentService.getPaymentStatistics();
      res.json(statistics);
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  }
  
  module.exports = {
    createPayment,
    updatePaymentStatus,
    getPaymentStatistics
  };