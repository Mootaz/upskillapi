const { Configuration, OpenAIApi } = require("openai");
const readline = require("readline");
const config = new Configuration({
  organization: "org-OuoSOrE08qUaD7Rq5OXVCfEX",
  apiKey: "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"
});
const openai = new OpenAIApi(config);

async function generateAssistantResponse(req, res) {
  try {
    const question = req.body.question;
    openai
    .createCompletion({
      model: "gpt-3.5-turbo",
      prompt: question,
      max_tokens: 4000,
      temperature: 0,
    })
    .then((response) => {
      // Handle the response

      // ...
    })
    .then((answer) => {
      // Handle the answer

      // ...
    })
    .then((answer) => {
      // Send the response to the client

      // ...
    })
    .catch((error) => {
      // Handle the error

      console.error("Error generating response:", error.response?.data || error.message);
      res.status(500).json({ error: error.message });
    });

  // ...
} catch (error) {
  // Handle any synchronous errors

  console.error("Error generating response:", error);
  res.status(500).json({ error: error.message });
}
}

module.exports = {
  generateAssistantResponse
};
