const bcrypt = require("bcrypt");
const { omit } = require("lodash");

// Models
const { User } = require("../../models/user");
const { UserCv } = require("../../models/cv");

// services
const { generateLoginToken } = require("../../service/identity/auth");
const { GetUserCurrentJob } = require("../../service/identity/userCurrentInfo");

module.exports = {
  authentificate: async (req, res, next) => {
    try {
      // get request data
      const { email, password } = req.body;
      // get the user by email
      const existingUser = await User.findOne({ email });
      if (!existingUser) {
        // return res.status(400).send({ error: "user-not-found" });
        // res.json(600, {
        //   error:"user-not-found"
        // });

        throw new Error(`processing error in request `);
      }
      if (!existingUser.enabled) {
        return res
          .status(400)
          .json({ message: "user banned by administrator" });
      }
      // verify password
      const isMatchingPassword = await bcrypt.compare(
        password,
        existingUser.password
      );
      if (!isMatchingPassword) {
        return res.status(400).json({ message: "password doesn't match" });
      }
      const authToken = await generateLoginToken(existingUser);

      const user = { ...omit(existingUser, "password"), authToken };

      // return user cv
      let cv;
      if (existingUser.cvId) {
        cv = await UserCv.findOne({ _id: user.cvId });
      }

      // return userRole
      const userRole = user.mainRole;

      const currentWork = GetUserCurrentJob(cv);
      user.currentWork = currentWork;
      return res.send({ user, cv, userRole });
    } catch (error) {
      return res.status(400).send({ error: "user-not-found" });
    }
  },
};
