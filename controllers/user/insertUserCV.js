// Models
const { UserCv } = require("../../models/cv");
const { User } = require("../../models/user");

module.exports = {
  insertUserCV: async (req, res, next) => {
    try {
      // get request data

      const user = req.body;

      /* ------------------------------- Validation ------------------------------- */
      const existingUser = await User.findOne({ _id: user._id });

      if (!existingUser) {
        return res.status(400).json({ message: "user-not-found" });
      }

      const personalInfo = user.userPersonalInfo;

      let usercvId;
      if (existingUser.cvId) {
        // the userHas already a cv
        usercvId = existingUser.cvId;
        // get the cv

        await UserCv.updateOne(
          { _id: existingUser.cvId },
          { $set: { ...personalInfo, ...user.userCVInfo } }
        );
        // update the user

        await User.updateOne({ _id: user._id }, { $set: { ...personalInfo } });
      } else {
        // insert the cv
        const cv = new UserCv({
          ...user.userPersonalInfo,
          ...user.userCVInfo,
        });
        cv.save();
        usercvId = cv._id;
        // update the user
        await User.updateOne(
          { _id: user._id },
          { $set: { cvId: cv._id, ...personalInfo } }
        );
      }

      /* ----------------------------- Implementation ----------------------------- */

      const updatedUser = await User.findOne({ _id: user._id });
      const userCV = await UserCv.findOne({ _id: usercvId });

      res.send({ cv: userCV, updatedUser });
      console.debug("user infos added or updated with success");
    } catch (error) {
      console.log("error", error);
      next(error);
    }
  },
};
