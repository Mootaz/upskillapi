const bcrypt = require("bcrypt");
const { User } = require("../../models/user");
const jwt = require("jsonwebtoken");

module.exports = {
  resetPassword: async (req, res, next) => {
    const { token, password } = req.body;

    var decoded = jwt.decode(token, {
      complete: true,
    });
    //cryptage password
    //generate a salt at level 10 strength
    const salt = await bcrypt.genSalt(10);
    //generate hashcode
    const hash = await bcrypt.hash(password, salt);
    const user = {
      password: hash,
    };
    User.updateOne({ email: decoded.payload.email }, { $set: user })
      .then(() => {
        res.status(201).json({
          message: "Thing updated successfully!",
        });
        user.save();
      })
      .catch((error) => {
        res.status(400).json({
          error: error,
        });
      });
  },
};
