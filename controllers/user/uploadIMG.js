// dependencies
const multer = require("multer");
const path = require("path");

module.exports = {
  uploadIMG: async (req, res, next) => {
    try {
      const storage = multer.diskStorage({
        destination: function (req, file, cb) {
          cb(null, "./uploads");
        },
        filename: function (req, file, cb) {
          cb(null, "vv" + Date.now() + "-" + path.extname(file.originalname));
        },
      });
  
      const upload = multer({ storage: storage });
  
      const fileUpload = upload.single("img");
  
      fileUpload(req, res, function (err) {
        if (err) {
          console.log("error", err);
          res.send("Error uploading file.");
        } else {
          if (!req.file) {
            console.log("No file uploaded.");
            res.send("No file uploaded.");
          } else {
            console.log("File uploaded successfully.");
            res.send("File uploaded successfully.");
          }
        }
      });
    } catch (error) {
      console.log("error", error);
    }
  },
  
};
