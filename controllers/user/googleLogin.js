const bcrypt = require("bcrypt");
const { omit } = require("lodash");
const { User } = require("../../models/user");
const { generateLoginToken } = require("../../service/identity/auth");

module.exports = {
  googleLogin: async (req, res, next) => {
    try {
      // get request data
      const { firstName, lastName, email, role, isForeignUser } = req.body;

      // if user exists retrn the user
      const existingUser = await User.findOne({ email });
      if (existingUser) {
        const authToken = await generateLoginToken(existingUser);
        return res.send({ ...omit(existingUser, "password"), authToken });
      } else {
        const password = Math.random().toString(36).slice(-8);
        //cryptage password

        //generate a salt at level 10 strength
        const salt = await bcrypt.genSalt(10);
        //generate hashcode
        const hash = await bcrypt.hash(password, salt);
        // get the user by email

        const user = new User({
          firstName,
          email,
          lastName,
          password: hash,
          roles: [role],
          isForeignUser: isForeignUser,
        });
        user.save();
        const authToken = await generateLoginToken(user);
        res.send({ ...omit(user, "password"), authToken });
      }
    } catch (error) {
      console.log("error", error);
      next(error);
    }
  },
};
