// Models
const { UserCv } = require("../../models/cv");
const { User } = require("../../models/user");

module.exports = {
  updateMentorServices: async (req, res, next) => {
    try {
      // get request data

      const { userId } = req.body;
      const { services } = req.body;

      /* ------------------------------- Validation ------------------------------- */
      const user = await User.findOne({ _id: userId });

      await User.updateOne(
        { _id: userId },
        { $set: { mentorServices: services } }
      );

      /* ----------------------------- Implementation ----------------------------- */

      const updatedUser = await User.findOne({ _id: userId });

      res.send({ updatedUser });
      console.debug("user services updated with success");
    } catch (error) {
      console.log("error", error);
      next(error);
    }
  },
};
