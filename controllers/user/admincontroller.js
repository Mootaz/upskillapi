
const adminService = require('../../service/Admin/adminService');

const adminController = {

    getAllUser: async (req, res) => {
        try {
            const adminuser = await adminService.getAlluser();
            res.json(adminuser);
        } catch (err) {
            res.status(500).json({ message: err.message })
        }
    },

    deleteAdmin: async (req, res) => {
        try {
            const adminuser = await adminService.deleteAdmin(req.params.id);
           
            if (adminuser == null) {
                return res.status(404).json({ message: 'Cannot find user' });
            }
            res.json(adminuser);
            
        } catch (error) {
            res.status(500).json({ message: error.message })
        }
    },

    getAllUserstatistics: async (req, res) => {
        try {
          const adminusers = await adminService.getAlluser();
      
          // Calculate statistics
          const statistics = adminusers.reduce((acc, user) => {
            const mainRole = user.mainRole;
            if (mainRole) {
              acc[mainRole] = (acc[mainRole] || 0) + 1;
            }
            return acc;
          }, {});
      
          // Add statistics to the response
          const response = {
            
            statistics: statistics,
          };
      
          res.json(response);
        } catch (err) {
          res.status(500).json({ message: err.message });
        }
      },

      getAllcoursestatistics: async (req, res) => {
        try {
          const adminusers = await adminService.getAllcourse();
      
          // Calculate statistics
          const statistics = adminusers.reduce((acc, course) => {
            const title = course.title;
            if (title) {
              acc[title] = (acc[title] || 0) + 1;
            }
            return acc;
          }, {});
      
          // Add statistics to the response
          const response = {
            
            statistics: statistics,
          };
      
          res.json(response);
        } catch (err) {
          res.status(500).json({ message: err.message });
        }
      }
      


}


module.exports = adminController;