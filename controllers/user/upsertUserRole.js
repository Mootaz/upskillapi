const { omit } = require("lodash");
const { User } = require("../../models/user");

module.exports = {
  upsertUserRole: async (req, res, next) => {
    try {
      // get request data

      const { userId, role } = req.body;
      /* ------------------------------- Validation ------------------------------- */
      const existingUser = await User.findOne({ _id: userId });
      
      if (!existingUser) {
        return res.status(400).json({ message: "user-doesn't-exist" });
      }
      /* ----------------------------- Implementation ----------------------------- */

      if (!existingUser.roles.includes(role)) {
        await User.updateOne(
          { _id: userId },
          { $set: { roles: [...existingUser.roles, role] } }
        );
      }
      const userDoc = await User.findOne({ _id: userId });

      res.send({ user: { ...omit(userDoc, "password") }, role: role });
    } catch (error) {
      console.log("error", error);
      next(error);
    }
  },
};
