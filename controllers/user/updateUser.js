const { User } = require("../../models/user");
module.exports = {
  updateUser: async (req, res, next) => {
    try {
      // get request data
      const { userId, updatedUser } = req.body;

      /* ------------------------------- Validation ------------------------------- */
      const existingUser = await User.findOne({ _id: userId });

      await User.updateOne({ _id: userId }, { $set: updatedUser });

      const userDoc = await User.findOne({ _id: userId });
      res.send(userDoc);
    } catch (error) {
      console.log("error", error);
      next(error);
    }
  },
};
