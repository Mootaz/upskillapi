const { User } = require("../../models/user");
const jwt = require("jsonwebtoken");
// dependencies
const { sendEmail } = require("../../service/email/sendMail");
module.exports = {
  forgotPassword: async (req, res, next) => {
    try {
      // get request data
      const { email } = req.body;

      // get the user by email
      const existingUser = await User.findOne({ email });
      //verify user exist
      if (existingUser) {
        //Tokenisation
        var tokenObject = {
          email: email,
        };
        var secret = email + "_" + new Date().getTime();
        var token = jwt.sign(tokenObject, secret);

        let mailOptions = {
          from: process.env.EMAIL,
          to: email,
          subject: "UpskillLabs",
          text: `http://localhost:3000/forgotPassword/${token}`,
        };

        //Methode of sending mail
        sendEmail({ ...mailOptions });
        return res.send(
          "An Email of renitialisation has been sent ! please check it"
        );
      } else return res.send("USER DOES NOT EXIST !");
    } catch (error) {
      console.log("error", error);
    }
  },
};
