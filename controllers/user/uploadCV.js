// dependencies
const multer = require("multer");
const { User } = require("../../models/user");

module.exports = {
  uploadCV: async (req, res, next) => {
    try {
      const { userId } = req.body;

      const filename = req.files[0]?.filename;

      const storage = multer.diskStorage({
        destination: function (req, file, cb) {
          cb(null, "./uploads");
        },
        filename: function (req, file, cb) {
          cb(null, file.filename);
        },
      });

      const upload = multer({ storage: storage });

      const fileUpload = upload.single("file");

      fileUpload(req, res, async function (err) {
        await User.updateOne(
          { _id: userId },
          { $set: { cvPath: `/uploads/${filename}` } }
        );
      });
    } catch (error) {
      console.log("error", error);
    }
  },
};
