const { User } = require("../../models/user");
const { omit } = require("lodash");
const { UserCv } = require("../../models/cv");
const { GetUserCurrentJob } = require("../../service/identity/userCurrentInfo");
module.exports = {
  getUser: async (req, res, next) => {
    try {
      // get request data
      const { userId } = req.query;

      // get the user by email
      const user = await User.findOne({ _id: userId }).lean();

      if (!user) {
        return res.status(400).json({ message: "user-not-found" });
      }
      const cv = await UserCv.findOne({ _id: user.cvId });
      const currentWork = GetUserCurrentJob(cv);
      user.currentWork = currentWork;

      return res.send({ user: { ...omit(user, "password") }, cv });
    } catch (error) {
      console.log("error", error);
      next(error);
    }
  },
};
