
const { User } = require("../../models/user");
module.exports = {
    banUser: async (req, res, next) => {
        try {
      // get request data
      const { userId } = req.body;

      /* ------------------------------- Validation ------------------------------- */
      const existingUser = await User.findOne({ _id:userId });
      await User.updateOne(
        { _id: userId },
        { $set: { enabled: !existingUser.enabled } }
      );

      const userDoc = await User.findOne({ _id: userId });
      res.send(userDoc);


    } catch (error) {
        console.log("error", error);
        next(error);
      }
    },
  };
  