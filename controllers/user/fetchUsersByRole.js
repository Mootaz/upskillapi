const { omit } = require("lodash");
const { User } = require("../../models/user");

module.exports = {
  fetchUsersByRole: async (req, res, next) => {
    try {
      // get request data
      const { role } = req.query;

      // get the user by email
      const userDocuments = await User.find({ roles: role });
      const users = userDocuments.map((user) => omit(user, ["password"]));
      return res.send(users);
    } catch (error) {
      console.log("error", error);
      next(error);
    }
  },
};
