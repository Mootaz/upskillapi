const { User } = require("../../models/user");
const { omit } = require("lodash");
const { UserCv } = require("../../models/cv");
module.exports = {
  getUserList: async (req, res, next) => {
    try {

      // get the user by email
      const users = await User.find();
   
      return res.send(users);
    } catch (error) {
      console.log("error", error);
      next(error);
    }
  },
};
