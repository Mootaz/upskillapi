const bcrypt = require("bcrypt");
const { omit } = require("lodash");
const { User } = require("../../models/user");
const { generateLoginToken } = require("../../service/identity/auth");

module.exports = {
  create: async (req, res, next) => {
    try {
      // get request data
      const { firstName, lastName, email, password, role } = req.body;
      /* ------------------------------- Validation ------------------------------- */
      const existingUser = await User.findOne({ email });
      if (existingUser) {
        return res.status(400).json({ message: "user-already-exists" });
      }
      /* ----------------------------- Implementation ----------------------------- */

      //cryptage password

      //generate a salt at level 10 strength
      const salt = await bcrypt.genSalt(10);
      //generate hashcode
      const hash = await bcrypt.hash(password, salt);
      // get the user by email

      const user = new User({
        firstName,
        email,
        lastName,
        password: hash,
        roles: [role],
        mainRole: role,
      });
      const authToken = await generateLoginToken(user);
      user.save();
      const userDocument = { ...omit(user, "password"), authToken };
      res.send(userDocument);
    } catch (error) {
      console.log("error", error);
      next(error);
    }
  },
};
