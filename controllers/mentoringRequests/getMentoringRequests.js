const { MentoringRequest } = require("../../models/mentoringRequests");
const { Message } = require("../../models/message");
const { User } = require("../../models/user");
module.exports = {
  getMentoringRequests: async (req, res, next) => {
    try {
      // get request data

      const { status, senderId, mentorId } = req.query;

      // get the user by email
      let query = {};
      if (status) {
        query.status = status;
      }
      if (senderId) {
        query.senderId = senderId;
      }
      if (mentorId) {
        query.mentorId = mentorId;
      }

      const requestDocuments = await MentoringRequest.find(query);

      const decoratedMentoringRequests = await Promise.all(
        requestDocuments.map(async (requestDoc) => {
          const sender = await User.findOne({
            _id: requestDoc.senderId,
          });

          const mentor = await User.findOne({
            _id: requestDoc.mentorId,
          });
          const messages = await Message.find({
            referenceType: "mentoringRequest",
            referenceId: requestDoc._id,
          });

          return { ...requestDoc._doc, mentor, sender, messages };
        })
      );

      return res.send(decoratedMentoringRequests);
    } catch (error) {
      console.log("error", error);
      next(error);
    }
  },
};
