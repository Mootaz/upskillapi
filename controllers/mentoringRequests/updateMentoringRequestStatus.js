const { MentoringRequest } = require("../../models/mentoringRequests");
const {
  sendNotification,
} = require("../../service/notification/sendNotifications");

module.exports = {
  updateMentoringRequestStatus: async (req, res, next) => {
    try {
      // get request data

      const { status, mentoringRequestId } = req.body;
      /* ------------------------------- Validation ------------------------------- */
      // @todo : validation
      /* ----------------------------- Implementation ----------------------------- */

      const mentoringRequest = await MentoringRequest.findOne({
        _id: mentoringRequestId,
      });

      await MentoringRequest.updateOne(
        { _id: mentoringRequestId },
        { $set: { status } }
      );

      /* ---------------------------- send notification --------------------------- */
      let action;

      switch (status) {
        case "accepted":
          action = "accept";

          break;
        case "reject":
          action = "reject";

          break;
      }

      sendNotification({
        senderId: mentoringRequest.mentorId,
        receiverId: mentoringRequest.senderId,
        topic: "mentoringRequest",
        action,
      });

      const request = await MentoringRequest.findOne({
        _id: mentoringRequestId,
      });
      res.send(request);
    } catch (error) {
      console.log("error", error);
      next(error);
    }
  },
};
