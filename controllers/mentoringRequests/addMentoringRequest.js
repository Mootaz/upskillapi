// Pckage dependencies
const { addMonths, eachWeekOfInterval } = require("date-fns");
const { omit } = require("lodash");
// Collections
const { MentoringRequest } = require("../../models/mentoringRequests");

// helpers
const {
  sendNotification,
} = require("../../service/notification/sendNotifications");

module.exports = {
  addMentoringRequest: async (req, res, next) => {
    try {
      // get request data

      const { request } = req.body;

      /* ------------------------------- Validation ------------------------------- */
      // @todo : validation
      /* ----------------------------- Implementation ----------------------------- */
      let dates;
      if (request.serviceIdentifier === "development-journey") {
        const endDate = addMonths(new Date(request.date), request.period);

        const mentoringDates = eachWeekOfInterval({
          start: new Date(request.date),
          end: endDate,
        
        });
        dates = mentoringDates;
      } else {
        dates = [request.date];
      }
      const requestDocument = { ...omit(request, "date"), dates };
      const mentoringRequest = new MentoringRequest(requestDocument);

      mentoringRequest.save();

      sendNotification({
        senderId: request.senderId,
        receiverId: request.mentorId,
        topic: "mentoringRequest",
        action: "submit",
      });
      res.send("done");
    } catch (error) {
      console.log("error", error);
      next(error);
    }
  },
};
