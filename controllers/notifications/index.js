const rootDirectory = __dirname;
const fs = require("fs");

module.exports = {};

fs.readdirSync(rootDirectory).forEach((file) => {
  if (file !== "index.js") {
    module.exports = {
      // eslint-disable-next-line
      ...require(`${rootDirectory}/${file}`),
      ...module.exports,
    };
  }
});
