const { Notification } = require("../../models/notification");
const { User } = require("../../models/user");

module.exports = {
  getUserNotificationsList: async (req, res, next) => {
    try {
      const { userId } = req.query;

      const userNotifications = await Notification.find({ receiverId: userId });
      const decoratedNotifications = await Promise.all(
        userNotifications.map(async (notif) => {
          const sender = await User.findOne({ _id: notif.senderId }).select({
            firstName: 1,
            lastName: 1,
            _id: 1,
          });
          const receiver = await User.findOne({ _id: notif.receiverId }).select(
            {
              firstName: 1,
              lastName: 1,
              _id: 1,
            }
          );
          return { ...notif._doc, sender, receiver };
        })
      );

      return res.send(decoratedNotifications);
    } catch (error) {
      console.log("error", error);
      next(error);
    }
  },
};
