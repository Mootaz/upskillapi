const courseService = require('../../service/course/courseService');
const Media = require("../../models/course/Media");
const UserService = require("../../service/userService")
const fs = require("fs");
const path = require("path");
const Course = require('../../models/course');
const { User } = require('../../models/user');


const courseController = {
  getAllCourses: async (req, res) => {
    try {
      const courses = await courseService.getAllCourses();
      res.json(courses);
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  },

  getCourseById: async (req, res) => {
    try {
      const course = await courseService.getCourseById(req.params.id);

      if (course == null) {
        return res.status(404).json({ message: 'Cannot find course' });
      }

      res.json(course);
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  },

  getMostLikedCourses: async (req, res) => {
    try {
      const courses = await courseService.getMostLikedCourses();
      res.json(courses);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  },

  createCourse: async (req, res) => {
    try {
      const course = await courseService.createCourse(
        req.body.title,
        req.body.description,
        req.body.category,
        req.body.price,
        req.body.image,
        req.body.media,
        req.body.User_id // Add the new field to the parameters
      );

      res.status(201).json(course);
    } catch (err) {
      res.status(400).json({ message: err.message });
    }
  },

  getRecommendedCourses: async (req, res) => {
    try {
      const category = req.params.category;
      const recommendedCourses = await courseService.getRecommendedCourses(category);
      res.json(recommendedCourses);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  },


  updateCourse: async (req, res) => {
    try {
      const course = await courseService.updateCourse(
        req.params.id,
        req.body.title,
        req.body.description,
        req.body.category,
        req.body.price,
        req.body.image // Add the new field to the parameters
      );
      res.json(course);
    } catch (err) {
      res.status(400).json({ message: err.message });
    }
  },

  deleteCourse: async (req, res) => {
    try {
      const course = await courseService.deleteCourse(req.params.id);

      if (course == null) {
        return res.status(404).json({ message: 'Cannot find course' });
      }

      res.json(course);
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  },

  uploadimg: async (req, res, next) => {
    try {
      const storage = multer.diskStorage({
        destination: function (req, file, cb) {
          cb(null, "./uploads");
        },
        filename: function (req, file, cb) {
          cb(null, "vv" + Date.now() + "-" + path.extname(file.originalname));
        },
      });

      const upload = multer({ storage: storage });

      const fileUpload = upload.single("img");

      fileUpload(req, res, function (err) {
        if (err) {
          console.log("error", err);
          res.send("Error uploading file.");
        } else {
          if (!req.file) {
            console.log("No file uploaded.");
            res.send("No file uploaded.");
          } else {
            console.log("File uploaded successfully.");
            res.send("File uploaded successfully.");
          }
        }
      });
    } catch (error) {
      console.log("error", error);
    }
  },
  getAll: async (req, res) => {
    try {
      const media = await Media.find();

      res.json(media);
    } catch (error) {
      console.log(error);
      res.status(400).json(error);
    }
  },


  create: async (req, res) => {
    const { name } = req.body;
    const { resume } = req.body;
    const { User_id, course_id } = req.body;

    console.log({ u: User_id, c: course_id, gf: res })


    let videosPaths = [];

    if (req.files && Array.isArray(req.files.videos) && req.files.videos.length > 0) {
      for (let video of req.files.videos) {
        videosPaths.push("/" + video.path);
      }
    }

    try {
      const createdMedia = await Media.create({
        name,
        videos: videosPaths,
        resume,
        User_id,
        course_id,

      });

      const media1 = await createdMedia.save();
      console.log(User_id);
      const User = await UserService.getUserByIdMedia(User_id);
      User.Files.push(media1._id);
      await User.save();
      res.json({ message: "Media created successfully", createdMedia });
    } catch (error) {
      console.log(error);
      res.status(400).json(error);
    }

  },



  getByIdMedia: async (req, res) => {
    try {
      const { id } = req.params;

      // Find the media by ID in the database
      const media = await Media.findById(id);

      if (!media) {
        // If media is not found, return an error response
        return res.status(404).json({ error: "Media not found" });
      }

      // Return the media as a response
      res.json(media);
    } catch (error) {
      // Handle any errors that occur during the process
      console.log(error);
      res.status(500).json({ error: "Internal server error" });
    }
  },

  getByCourseIdMedia: async (req, res) => {
    try {
      const { courseId } = req.params;

      // Find the course in the database
      const course = await Course.findById(courseId);

      if (!course) {
        // If course is not found, return an error response
        return res.status(404).json({ error: "Course not found" });
      }

      // Find the media associated with the course
      const media = await Media.find({ course_id: course._id });


      // Return the course and its associated media as a response
      res.json({ media });
    } catch (error) {
      // Handle any errors that occur during the process
      console.log(error);
      res.status(500).json({ error: "Internal server error" });
    }
  }
  ,


  delete: async (req, res) => {
    const { id } = req.params;

    try {
      const media = await Media.findById(id);

      if (!media) {
        return res.status(404).json({ message: "Media not found" });
      }

      // Delete video files from server
      for (let videoPath of media.videos) {
        const filePath = path.join(__dirname, "..", "public", "videos", videoPath);

      }


      // Delete the media object
      await Media.findByIdAndDelete(id);

      res.json({ message: "Media deleted successfully" });
    } catch (error) {
      console.log(error);
      res.status(400).json(error);
    }
  },

  Recommendedcourse: async (req, res) => {
    try {
      const { userId } = req.params;
  
      // Fetch user's interests from the database
      const user = await User.findById(userId);
  
      // Check if user exists
      if (!user) {
        return res.status(404).json({ error: 'User not found' });
      }
  
      // Find courses matching user interests
      const recommendedCourses = await Course.find({ category: { $in: user.interests } });
  
      // Extract unique categories from recommended courses
      const uniqueCategories = Array.from(new Set(recommendedCourses.map(course => course.category)));
  
      // Add recommended course categories to user's interests
      user.interests = [...user.interests, ...uniqueCategories];
  
      // Save the updated user data to the database
      await user.save();
  
      res.json(recommendedCourses);
    } catch (error) {
      console.error('Error getting recommended courses:', error);
      res.status(500).json({ error: 'An error occurred' });
    }
  },
  
  async  addRecommendationToUser(req, res) {
    const { userId, courseId } = req.params;
  
    try {
      await courseService.addRecommendationToUser(userId, courseId);
      res.json({ message: 'Recommendation added successfully' });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  },

async  deleteRecommendationFromUser(req, res) {
  const { userId, courseId } = req.params;

  try {
    await courseService.deleteRecommendationFromUser(userId, courseId);
    res.json({ message: 'Recommendation deleted successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
},


  async  getUserRecommendations(req, res) {
    const { userId } = req.params;
  
    try {
      const recommendations = await courseService.getUserRecommendations(userId);
      res.json(recommendations);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  }
};




module.exports = courseController;
