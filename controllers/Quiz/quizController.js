// questionController.js

const questionService = require('../../service/Quiz/QuizService');

async function getAllQuestions(req, res) {
  try {
    const questions = await questionService.getAllQuestions();
    res.json(questions);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
}

async function createQuestion(req, res) {
  const { question, options, correctOption, User_id } = req.body;
  const questionData = { question, options, correctOption, User_id };

  try {
    const newQuestion = await questionService.createQuestion(questionData);
    res.status(201).json(newQuestion);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
}

async function getQuestionById(req, res) {
  const questionId = req.params.id;

  try {
    const question = await questionService.getQuestionById(questionId);
    if (question) {
      res.json(question);
    } else {
      res.status(404).json({ error: 'Question not found' });
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
}


module.exports = {
  getAllQuestions,
  createQuestion,
  getQuestionById
};
