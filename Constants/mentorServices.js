const mentorServices = [
  {
    name: "visual coffee",
    identifier: "visual-coffee",
    time: 15,
    priceLevels: "Free",
  },
  {
    name: "Breakthrough session",
    identifier: "breakthrough-session",
    time: 45,
    priceLevels: ["0 - 49.99", "50 - 99.99", "100 - 199.99", "200 <"],
  },
  {
    name: "Extentded breakthrough session",
    identifier: "extednde-breakthrough-session",
    time: 120,
    priceLevels: ["0 - 49.99", "50 - 99.99", "100 - 199.99", "200 <"],
  },
  {
    name: "Development journey",
    identifier: "development-journey",
    priceLevels: ["0 - 49.99", "50 - 99.99", "100 - 199.99", "200 <"],
    time: 120,
    duration: 2, // the unit is month for duration and minutes for time
  },
];
module.exports = { mentorServices };
