const mongoose = require('mongoose');


const DislikeSchema = new mongoose.Schema({
    User_id:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'User',
        required:true
    },
    Video_id:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Media',
        required:true
    }
  
});

const Dislike = mongoose.model('Dislike', DislikeSchema);

module.exports = Dislike;
