const mongoose = require('mongoose');


const LikeSchema = new mongoose.Schema({
    User_id:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'User',
        required:true
    },
    Video_id:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Media',
        required:true
    }
  
});

const Like = mongoose.model('Like', LikeSchema);

module.exports = Like;
