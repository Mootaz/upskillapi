const mongoose = require("mongoose");




const ReplySchema = new mongoose.Schema({
  text: String,
  User_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  createdAt: { type: Date, default: Date.now },
  date: String
});

const CommentSchema = new mongoose.Schema({
  text: String,
  User_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  createdAt: { type: Date, default: Date.now },
  date: String,
  replies: [ReplySchema]
});

const MediaSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true
    
    },
    resume: {
      type: String,
      required: true
    },
    
    course: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Course'
    },
    User_id:{
      type:mongoose.Schema.Types.ObjectId,
      ref:'User',
      required:true
  },
  Likes:[{
    type:mongoose.Schema.Types.ObjectId,
    ref:"Like"
  }],
    videos: [{ type: String }],
    course_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Course' },
    comments: [CommentSchema],
    likeCount: { type: Number, default: 0 },// Add the likeCount field
  },

  {
    timestamps: true,
  }
);

module.exports = Media = mongoose.model("Media", MediaSchema);


