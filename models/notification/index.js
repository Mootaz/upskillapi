const mongoose = require("mongoose");

// Schema
const notificationSchema = new mongoose.Schema({
  senderId: { type: String, required: true },
  receiverId: { type: String, required: true },
  topic: { type: String, required: true },
  sentAt: { type: String, required: true },
  seen: { type: Boolean, default: false },
  action: { type: String },
});
const Notification = mongoose.model("Notification", notificationSchema);
module.exports.Notification = Notification;
