const mongoose = require("mongoose");

//Schema
const mentoringRequestSchema = new mongoose.Schema({
  senderId: { type: String, required: true },
  mentorId: { type: String, required: true },
  subject: { type: String, required: true },
  serviceIdentifier: { type: String, required: true },
  dates: [{ type: String }],
  status: { type: String, required: true, default: "pending" },
  price: { type: Number },
  title: { type: String },
  time: { type: String },
  period: { type: Number },
});
const MentoringRequest = mongoose.model(
  "MentoringRequest",
  mentoringRequestSchema
);
module.exports.MentoringRequest = MentoringRequest;
