const mongoose = require("mongoose");
const { mentorServices } = require("../../Constants/mentorServices");

//Schema
const userSchema = new mongoose.Schema({
  firstName: String,
  lastName: String,
  linkedinLink: { type: String },
  password: { type: String, required: true },
  email: { type: String, require: true, index: { unique: true } },
  birthDate: { type: Date },
  nationalities: { type: Array },
  phone: { type: String },
  address: { type: String },
  nationality: { type: String },
  isForeignUser: { type: Boolean, default: false },
  driveLicence: { type: String },
  roles: { type: [String] },
  enabled: { type: Boolean, default: true },
  mainRole: { type: String },
  Paiement:[{
    type:mongoose.Schema.Types.ObjectId,
    ref:"Payment"
  }],
  recommendations: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Course'
  }],

  Dislikes:[{
    
    type:mongoose.Schema.Types.ObjectId,
    ref:"Dislike"
  }],

  Likes:[{
    
    type:mongoose.Schema.Types.ObjectId,
    ref:"Like"
  }],
  Files:[{
    type:mongoose.Schema.Types.ObjectId,
    ref:"Media"
  }],
  courses: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Course'
  }],
  cvId: { type: String },
  isMentorAvailable: { type: Boolean, default: true },
  mentorServices: {
    type: Array,
    default: mentorServices.map((service) => ({
      identifier: service.identifier,
      name: service.name,
      enabled: service.identifier === "visual-coffee" ? true : false,
    })),
  },
  mainRole: { type: String },
  cvPath: { type: String },
});

userSchema.statics.findOneById = function(userId) {
  return this.findOne({ _id: userId });
};
const User = mongoose.model('User', userSchema);
module.exports.User = User;
