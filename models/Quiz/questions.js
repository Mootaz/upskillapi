// Question.js

const mongoose = require('mongoose');

const questionSchema = new mongoose.Schema({
  question: {
    type: String,
    required: true,
  },
  options: {
    type: [String],
    required: true,
  },
  User_id:{
    type:mongoose.Schema.Types.ObjectId,
    ref:'User',
    required: true,
},
  correctOption: {
    type: Number,
    required: true,
  },
});

module.exports = mongoose.model('Question', questionSchema);
