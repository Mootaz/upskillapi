const mongoose = require("mongoose");
//schema
const messageSchema = new mongoose.Schema({
  sender: { type: String, required: true },
  receiver: { type: String, required: true },
  referenceType: { type: String },
  referenceId: { type: String },
  file: { type: Array },
  parentId: { type: String },
  text: { type: String },
  sentAt: { type: Date },
});
const Message = mongoose.model("Message", messageSchema);
module.exports.Message = Message;
