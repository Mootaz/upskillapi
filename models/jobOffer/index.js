const mongoose = require("mongoose");
const jobOfferSchema = new mongoose.Schema({
  recruiterId: String,
  title: { type: String },
  description: { type: String },
});
const JobOffer = mongoose.model("JobOffer", jobOfferSchema);
module.exports.JobOffer = JobOffer;
