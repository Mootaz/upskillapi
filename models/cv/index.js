const mongoose = require("mongoose");
const CVSchema = new mongoose.Schema({
  aboutMe: String,
  skills: { type: Array, default: [] },
  work: { type: Array, default: [] },
  internships: { type: Array },
  diplomas: { type: Array },
  trainings: { type: Array },
  facebookLink: String,
  whatsappLink: String,
  linkedinLink: String,
  instagramLink: String,

  hobbies: { type: Array },
  languages: { type: Array },
  preferredSchedule: { type: Array },
});
const UserCv = mongoose.model("CV", CVSchema);
module.exports.UserCv = UserCv;
