const dialogflow = require('dialogflow')


const projectId = "test-chat-bot-app-389615"
const configuration = {
  credentials: {
    private_key: "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDXH3tnC8OZ9Wld\n82VQMj7BvCLU6yl2KHKxLC7KxF1+YFt/NKdEVKenii+e8M0vDWUyRU2i3Eng8EVG\n/VQ47R5sCA74bxltVDGc23SLNoeZU9IYqRCearCBYnip3sEPtFROzZ9mLqbgfhMi\ngNCRv/t3Ziw1iB5w/ttWNNs06OLwyRz7JhjXbeiQNIw5BdRfDr1fzhHJ/L+x2NSP\nwyxPpmTq+1SswZorJp7IOLhK9mupeAESuPk36XlAxp8RJWfBhOPcpLHGAgFeQs4H\njUt6rVXlosW/D7cAMB+eOSzxPLcbSVMTwtXoPBN9Llbq1niodAd3KaEZ6hFTMSn0\nNACRSxFZAgMBAAECggEACiKLkWeprkYBOGuDj557LzlwrENTNfCvgDVd4JbiKLzb\n0q3/K2Ypnk6rJUigm658raLVbSGfwWwDqrwiQBEiNWJaNWe89O+m64YgHLO6LVKy\nCHaYN9gJVMjE31i4ei4h9eYUgNK115TymQkklzLj4zr6RKAssi1WCN7lbPEdfMzt\nBauHhxYdQ81i2beNQeiDvF+gW9ZnNuaaa0wd0slvj+rciDbZWOU6+/cMpAftd8L3\nOIVchxIUWFyDGTp+kYzesVNdtlymbqO1LwQm1CFbb80BfyAr54p94ozkcVrQFPXL\nXTXP7zW11u/nbize6/3KBWivXb9ltap3yu2OcqJU+QKBgQD/WKQRvH7RoJjK4OZi\nDXGYfXAcUS7ZK0xWj/JcCvqnv/jo0rJNG+sUj7q6k245aDEtnQaI5HRIl5nT1m3c\nB67BgTdt7KRHKG27QJ1l9/ZzyAaUFQ7j4CILozb0ERyJHXeOjAiWRNG2PPzhPw1Q\nASaU67267+Mmbev3QSAiqjJ7RwKBgQDXrHpd6b80GEftMj+Ro1nhyMiOeRKhZrnA\nmklf5Z2D8iExuVgZ6D/DCFV6yq/4LQKXNWAJWKXCQF58jO7yswST/0GzI9MQuula\nDggNrt/pN2iuSXRaKdbF/V2CWTTJ4qrnmglZ/G0poCka02SUANJwDrItd03m/lQI\n1ENpJXseXwKBgQCLgAXCmtQ9+hr/W3cBwo543K7PCZsqkfkWoXWqy2S9GKgHqnQ2\npDX9+bfuDnV9b2rOnKbhSs4sioZDsTY7qwbSOKHrsAjfxzqLvnU/LAS8pF7Tr91+\nqQhwiy6OkHqifjK+8coPLnHTueX9uNCxEK/fVBtHcZ8Lc6oiXrYLuifyKQKBgDpZ\ntMWhzxRGGXeoJ6C8hD3c973sLckzJMBiPcXPNr2TDb4JOjfQKSgYIiBVpCRN8Sx9\nyVzd+gLDLY0fCCrrmip8Xgccgyc8mpO9xhoFnRjY395/ixY1yaDJerogbnLMe5ar\niyoobFRdu6FBXi3YxxaAM16vr4j2eD/nRdG1zUibAoGBAMdbH7GRTY6XfxyuneQU\np8rIKKvedj3VQxv4aZ5iyGP8+wlNUYah1QfbZVp8goGzRPT3KJ/Rb8EiVnNOhvcO\nXQRJLMKLNDdvz2A/ew0dJtauqjXcith3tjL/zifJJnanwlPttSaWmELEt74FrFSF\ns8YY08PaFn3SnOnwHnyCnnNR\n-----END PRIVATE KEY-----\n"
   , client_email: "chat-bot-app-test@test-chat-bot-app-389615.iam.gserviceaccount.com",
  },
}

const sessionId = '997753'
const languageCode = 'en-US'
const sessionClient = new dialogflow.SessionsClient(configuration)

const sessionPath = sessionClient.sessionPath(projectId, sessionId)

const talkToChatbot = async (message) => {
  console.log('message ' + message)
  const botRequest = {
    session: sessionPath,
    queryInput: {
      text: {
        text: message,
        languageCode: languageCode,
      },
    },
  }

  let response = await sessionClient
    .detectIntent(botRequest)
    .then((responses) => {
      //console.log(JSON.stringify(responses))

      const requiredResponse = responses[0].queryResult
      // console.log(requiredResponse)
      return requiredResponse
    })
    .catch((error) => {
      console.log('ERROR: ' + error)
    })

  return response
}

module.exports = talkToChatbot
