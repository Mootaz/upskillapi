const Like = require('../../models/course/Like');
const Media = require('../../models/course/Media');
const mongoose = require('mongoose');
const { User } = require('../../models/user');

async function like(userId, videoId) {
  try {
    const existingLike = await Like.findOne({ User_id: userId, Video_id: videoId });

    if (existingLike) {
      // If the user already liked the video, remove the like and decrement likeCount
      await Like.deleteOne({ User_id: userId, Video_id: videoId });
      await Media.updateOne({ _id: videoId }, { $pull: { Likes: existingLike._id }, $inc: { likeCount: -1 } });
      console.log('Like removed successfully');
    } else {
      // If the user hasn't liked the video, add the like and increment likeCount
      const newLike = new Like({
        User_id: userId,
        Video_id: videoId
      });

      const savedLike = await newLike.save();
      await Media.updateOne({ _id: videoId }, { $push: { Likes: savedLike._id }, $inc: { likeCount: 1 } });
      console.log('Like saved successfully:', savedLike);
      return savedLike;
    }
  } catch (error) {
    console.error('Error saving/removing like:', error);
    throw error;
  }
}


async function fetchMostPopularComment(videoId) {
  try {
    if (!mongoose.Types.ObjectId.isValid(videoId)) {
      throw new Error('Invalid videoId');
    }

    const media = await Media.findById(videoId).populate({
      path: 'comments',
      populate: { path: 'likes' }
    });

    if (!media) {
      throw new Error('Media not found');
    }

    const popularComments = media.comments.filter(comment => comment.likes && comment.likes.length > 0);

    if (popularComments.length === 0) {
      throw new Error('No popular comments found');
    }

    const mostPopularComment = popularComments.sort((a, b) => b.likes.length - a.likes.length)[0];

    const likedUserIds = mostPopularComment.likes.map(like => like.userId);
    const likedUsers = await User.find({ _id: { $in: likedUserIds } });

    console.log('Most popular comment:', mostPopularComment);
    console.log('Liked users:', likedUsers);

    return mostPopularComment;
  } catch (error) {
    console.error('Error fetching most popular comment:', error);
    throw error;
  }
}

















  
  module.exports = {
    like
   
  };

  
  
  
  
  
  