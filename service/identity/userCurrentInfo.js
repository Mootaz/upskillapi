const { compareExperienceByStartDate } = require("../helper/CompareDate");

module.exports = {



    /**
    * Takes the current job
    * 
    */
    GetUserCurrentJob: (UserCv) => {
        let currentWork;
        if (UserCv?.work?.length > 0) {
            const currentjobs = UserCv?.work.filter(work => work.isCurrentWork);
            if (currentjobs.length === 0) {
                return currentWork;
            }
            if (currentjobs.length === 1) {
                return currentjobs[0].title;
            }
            // sort phases with generated dates by dates
            const sortedExperiences = currentjobs.sort(
                function (exp1, exp2) {
                    return compareExperienceByStartDate(
                        exp1.startDate,
                        exp2.startDate,
                    );
                },
            )
            currentWork = sortedExperiences[0].title;
            return currentWork;
        }

        return currentWork;
    },

};