// Dependencies

const jwt = require("jsonwebtoken");

// Models

// Constants and configs
const secret = "kjzheazgh"; //process.env.JWT_SECRET;
const AUTHENTICATION_TOKEN_EXPIRATION_TIME = "720h";

module.exports = {
  /**
   * Generates an authentication token based on the client app
   * and the user roles
   * @param {Object} user
   */
  generateLoginToken: async (user) => {
    // Sign the JWT by assigning user roles
    return jwt.sign(
      {
        id: user.id,
        email: user.email,
        // @Todo: add user roles and permission
      },
      secret,
      {
        algorithm: "HS256",
        expiresIn: AUTHENTICATION_TOKEN_EXPIRATION_TIME,
      }
    );
  },

  /**
   * Verifies wether there's a valid user behind the decoded token
   * @param {*} decoded
   */
  isValidLoginToken: async (decoded) => {
    const { id } = decoded;
    const userLookup = await User.findByPk(id);
    if (!userLookup) {
      return { isValid: false };
    }
    return { isValid: true };
  },
};
