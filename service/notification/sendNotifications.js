const { Notification } = require("../../models/notification");

// lib dependecies
const { format } = require("date-fns");

module.exports = {
  /**
   * sendesNotifications
  
   */
  sendNotification: (notification) => {
    const notificationObject = new Notification({
      ...notification,
      sentAt: format(new Date(), "dd/MM/yyyy"),
    });

    notificationObject.save();
  },
};
