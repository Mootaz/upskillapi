const Course = require('../../models/course');
const Media = require('../../models/course/Media');
const { User } = require('../../models/user');
const NearestNeighbors = require('nearest-neighbors');


const courseService = {
  getAllCourses: async () => {
    return await Course.find();
  },

  getCourseById: async (id) => {
    try {
      // Find the course by ID in the database and populate the media field
      const course = await Course.findById(id).populate('media');
  
      if (!course) {
        // If course is not found, return an error response
        throw new Error('Course not found');
      }
  
      return course;
    } catch (error) {
      // Handle any errors that occur during the process
      console.log(error);
      throw new Error('Internal server error');
    }
  }
  ,

  getMostLikedCourses: async () => {
    try {
      const courses = await Course.aggregate([
        {
          $lookup: {
            from: 'media',
            localField: 'media',
            foreignField: '_id',
            as: 'media',
          },
        },
        {
          $addFields: {
            likeCount: { $sum: '$media.likeCount' },
          },
        },
        {
          $sort: { likeCount: -1 },
        },
      ]);
  
      return courses;
    } catch (error) {
      console.log(error);
      throw new Error('Internal server error');
    }
  },
  


  
  

  createCourse: async (title, description, category, price, image, media, User_id) => {
    const course = new Course({
      title: title,
      description: description,
      category: category,
      User_id: User_id,
      price: price,
      image: image,
      media: media // Add the new field to the object
    });
  
   const course1= await course.save();
   const Media = await courseService.getCourseById(id);
   course1.media.push(Media._id);
   await Media.save();
    return course
  }
  ,

  updateCourse: async (id, title, description, category, price, image) => {
    const course = await Course.findById(id);

    if (course == null) {
      throw new Error('Cannot find course');
    }

    course.title = title;
    course.description = description;
    course.category = category;
    course.price = price;
    course.image = image; // Update the field in the object

    return await course.save();
  },

  deleteCourse: async (id) => {
    const course = await Course.findById(id);

    if (course == null) {
      throw new Error('Cannot find course');
    }

    return await course.remove();
  },


  async addRecommendationToUser(userId, courseId) {
    const user = await User.findOneById(userId);
  
    if (!user) {
      throw new Error('User not found');
    }
  
    // Add the course recommendation to the user's recommendations array
    user.recommendations.push(courseId);
    await user.save();
  },

async  deleteRecommendationFromUser(userId, courseId) {
  const user = await User.findOneById(userId);

  if (!user) {
    throw new Error('User not found');
  }
  // Remove the course recommendation from the users recommendations array
  user.recommendations = user.recommendations.filter((rec) => rec.toString() !== courseId);
  await user.save();
},


  async  getUserRecommendations(userId) {
    const user = await User.findOneById(userId)
      .populate({
        path: 'recommendations',
        populate: {
          path: 'recommendedCourses',
          model: 'Course'
        }
      })
      .exec();

 
  
    if (!user) {
      throw new Error('User not found');
    }
  
    return user.recommendations;
  }
  
  
  
  




  
  
  
};

module.exports = courseService;
