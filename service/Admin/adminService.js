const {User} = require('../../models/user');
const Course = require('../../models/course');

const adminService = {
  
getAlluser: async () => {
  return await User.find();
},
getAllcourse: async () => {
  return await Course.find();
},


deleteAdmin: async (id) => {
  const admin = await User.findById(id);

  if (admin == null) {
    throw new Error('Cannot find user');
  }

  return await admin.remove();
},
  };
  
  module.exports = adminService;
