const {isBefore , isAfter} = require ('date-fns');
module.exports = {
    /**
 * Takes the max date
 * 
 */
    compareExperienceByStartDate: (
        exp1Start,
        exp2Start,
    ) => {
        const exp1StartDate = new Date(exp1Start);
        const exp2StartDate = new Date(exp2Start);

        // sort by phases start date
        if (isBefore(exp1StartDate, exp2StartDate)) {
            return 1;
        }
        if (isAfter(exp1StartDate, exp2StartDate)) {
            return -1;
        }
        return 0;
    },
}