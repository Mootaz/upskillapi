const DisLike = require('../../models/course/Dislike');

async function Dislike(userId, videoId) {
  try {
    const existingLike = await DisLike.findOne({ User_id: userId, Video_id: videoId });

    if (existingLike) {
      // If the user already liked the video, remove the like and decrement likeCount
      await DisLike.deleteOne({ User_id: userId, Video_id: videoId });
      await Media.updateOne({ _id: videoId }, { $inc: { likeCount: -1 } });
      console.log('Like removed successfully');
    } else {
      // If the user hasn't liked the video, add the like and increment likeCount
      const newLike = new DisLike({
        User_id: userId,
        Video_id: videoId
      });

      const savedLike = await newLike.save();
      await Media.updateOne({ _id: videoId }, { $inc: { likeCount: 1 } });
      console.log('DisLike saved successfully:', savedLike);
      return savedLike;
    }
  } catch (error) {
    console.error('Error saving/removing like:', error);
    throw error;
  }
}





  
  module.exports = {
    Dislike,
   
  };

  
  
  
  
  
  