const Payment = require('../../models/payment');
const userService = require ('../../service/userService')
const stripe = require('stripe')('sk_test_51LT17fHFI0nvdg87Nhbx9q1HvbGRlL2uBxAPvyQKlCBfEkkPfXI0B70FFssFj75ongozYbe1GacpkwpoWl4xMrLM00dQxGb5ug');

async function createPayment(email, amount, currency, source, cvc, date, User_id) {
  // Retrieve or create a customer in Stripe based on the email
  let customer = await stripe.customers.list({
    email: email,
    limit: 1,
  });

  if (customer.data.length === 0) {
    customer = await stripe.customers.create({
      email: email,
    });
  } else {
    customer = customer.data[0];
  }

  const cardToken = await stripe.tokens.create({
    card: {
      number: source.cardNumber,
      exp_month: source.expMonth,
      exp_year: source.expYear,
      cvc: cvc,
    },
  });

  const paymentMethod = await stripe.paymentMethods.create({
    type: 'card',
    card: {
      token: cardToken.id,
    },
  });

  await stripe.paymentMethods.attach(paymentMethod.id, {
    customer: customer.id,
  });

  await stripe.customers.update(customer.id, {
    invoice_settings: {
      default_payment_method: paymentMethod.id,
    },
  });

  const paymentIntent = await stripe.paymentIntents.create({
    amount: amount * 100, // Amount in cents
    currency: currency,
    customer: customer.id,
    payment_method: paymentMethod.id,
    description: cvc,
    receipt_email: email,
  });

  const payment = new Payment({
    email,
    amount,
    currency,
    source: JSON.stringify(source),
    cvc,
    date,
    User_id,
    paymentIntentId: paymentIntent.id,
  });

  const payment1 = await payment.save();
  console.log("payment1", payment1);
  console.log("User_id ", User_id);

  const user = await userService.getUserById(User_id);
  console.log("user", user);
  user.Paiement.push(payment1._id);
  await user.save();

  return payment;
}






  async function updatePaymentStatus(paymentId, status) {
    const payment = await Payment.findById(paymentId);
    if (!payment) {
      throw new Error('Payment not found');
    }
  
    if (status === 'valid') {
      // Confirm the payment with the payment service provider
      const paymentIntent = await stripe.paymentIntents.confirm(payment.paymentIntentId);
  
      // Check if the payment was successful
      if (paymentIntent.status === 'succeeded') {
        // Update the payment status in your database to "Paid"
        payment.status = 'Paid';
        await payment.save();
      }
    } else {
      // If the payment is not valid, update the payment status accordingly
      payment.status = status;
      await payment.save();
    }
  
    return payment;
  }


  async function getPaymentStatistics() {
    try {
      const statistics = await Payment.aggregate([
        {
          $group: {
            _id: '$status',
            totalAmount: { $sum: '$amount' },
            count: { $sum: 1 },
          },
        },
      ]);
      return statistics;
    } catch (error) {
      throw new Error('Failed to retrieve payment statistics');
    }
  }
  
  
  
  module.exports = {
    createPayment,
    updatePaymentStatus,
    getPaymentStatistics
  };