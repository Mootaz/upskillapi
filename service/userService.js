const {User} = require('../models/user/');

const userService = {

  getUserById: async (id) => {
    return await User.findById(id).populate('Paiement');
  },
  getUserByIdMedia: async (id) => {
    return await User.findById(id).populate('Files');
  },
};

module.exports = userService;
