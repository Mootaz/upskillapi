// questionService.js

const Question = require('../../models/Quiz/questions')

async function getAllQuestions() {
  try {
    const questions = await Question.find();
    return questions;
  } catch (error) {
    throw new Error('Error retrieving questions');
  }
}

async function createQuestion(questionData) {
  try {
    const question = await Question.create(questionData);
    return question;
  } catch (error) {
    throw new Error('Error creating question');
  }
}

async function getQuestionById(questionId) {
    try {
      const question = await Question.findById(questionId);
      return question;
    } catch (error) {
      throw new Error('Error retrieving question');
    }
  }
  

module.exports = {
  getAllQuestions,
  createQuestion,
  getQuestionById
};
