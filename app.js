/* -------------------------------------------------------------------------- */
/*                                Dependencies                                */
/* -------------------------------------------------------------------------- */
const express = require("express");
const path = require('path');
const http = require('http');
const moment = require('moment');
const multer = require('multer');
const socketio = require('socket.io');
const app = express();
require("dotenv").config();
const mongoose = require("mongoose");
// const bodyParser = require("body-parser");
const cors = require("cors");
const Payment = require('./models/payment');
const Stripe = require('stripe')(process.env.SECRET_KEY);
const { v4: uuidv4 } = require('uuid');
const morgan = require('morgan')
// const os = require("os");
// const formData = require("express-form-data");
//********** */
const uploadRoutes = require('./routes/uploadRoutes.js');


//************** */
// // Define port
const PORT = process.env.PORT || 4000;

/* ------------------------------ Server config ------------------------------ */
// Parse application/json in req body
app.use(express.json());
app.use(cors());

// // Body parser for form data
// app.use(bodyParser.urlencoded({ extended: true }));
// app.use(bodyParser.json());
// const options = {
//   uploadDir: os.tmpdir(),
//   autoClean: true,
// };

// // parse data with connect-multiparty.
// app.use(formData.parse(options));
// // delete from the request all empty files (size == 0)
// app.use(formData.format());
// // change the file objects to fs.ReadStream
// app.use(formData.stream());
// // union the body and the files
// app.use(formData.union());
// // Treat requests

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
const server = http.createServer(app);

const io = socketio(server);

app.use((req, res, next) => {
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, x-Requested-With; Content-Type, Accept, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "GET, POST, PATCH, PUT, DELETE, OPTIONS"
  );
  res.setHeader("Access-Control-Allow-Credentials", "true");
  res.setHeader("Access-Control-Allow-Origin", "*");
  next();
});

app.use(express.static(path.join(__dirname, 'public')));
app.get('/socket.io/socket.io.js', (req, res) => {
  res.sendFile(__dirname + '/node_modules/socket.io-client/dist/socket.io.js');
});
let rooms = {};
let socketroom = {};
let socketname = {};
let micSocket = {};
let videoSocket = {};
let roomBoard = {};

io.on('connect', socket => {

    socket.on("join room", (roomid, username) => {

        socket.join(roomid);
        socketroom[socket.id] = roomid;
        socketname[socket.id] = username;
        micSocket[socket.id] = 'on';
        videoSocket[socket.id] = 'on';
      
        if (rooms[roomid] && rooms[roomid].length > 0) {
            rooms[roomid].push(socket.id);
            socket.to(roomid).emit('message', `${username} joined the room.`, `${username}`, moment().format(
                "h:mm a"
            ));
            io.to(socket.id).emit('join room', rooms[roomid].filter(pid => pid != socket.id), socketname, micSocket, videoSocket);
        }
        else {
            rooms[roomid] = [socket.id];
            io.to(socket.id).emit('join room', null, null, null, null);
        }

        io.to(roomid).emit('user count', rooms[roomid].length);

    });

    socket.on('action', msg => {
        if (msg == 'mute')
            micSocket[socket.id] = 'off';
        else if (msg == 'unmute')
            micSocket[socket.id] = 'on';
        else if (msg == 'videoon')
            videoSocket[socket.id] = 'on';
        else if (msg == 'videooff')
            videoSocket[socket.id] = 'off';

        socket.to(socketroom[socket.id]).emit('action', msg, socket.id);
    })
  
    socket.on('video-offer', (offer, sid) => {
        socket.to(sid).emit('video-offer', offer, socket.id, socketname[socket.id], micSocket[socket.id], videoSocket[socket.id]);
    })

    socket.on('video-answer', (answer, sid) => {
        socket.to(sid).emit('video-answer', answer, socket.id);
    })

    socket.on('new icecandidate', (candidate, sid) => {
        socket.to(sid).emit('new icecandidate', candidate, socket.id);
    })

    socket.on('message', (msg, username, roomid) => {
        io.to(roomid).emit('message', msg, username, moment().format(
            "h:mm a"
        ));
    })

    socket.on('getCanvas', () => {
        if (roomBoard[socketroom[socket.id]])
            socket.emit('getCanvas', roomBoard[socketroom[socket.id]]);
    });

    socket.on('draw', (newx, newy, prevx, prevy, color, size) => {
        socket.to(socketroom[socket.id]).emit('draw', newx, newy, prevx, prevy, color, size);
    })

    socket.on('clearBoard', () => {
        socket.to(socketroom[socket.id]).emit('clearBoard');
    });

    socket.on('store canvas', url => {
        roomBoard[socketroom[socket.id]] = url;
    })
       

    
    socket.on('disconnect', () => {
        if (!socketroom[socket.id]) return;
        socket.to(socketroom[socket.id]).emit('message', `${socketname[socket.id]} left the chat.`, `Bot`, moment().format(
            "h:mm a"
        ));
        socket.to(socketroom[socket.id]).emit('remove peer', socket.id);
        var index = rooms[socketroom[socket.id]].indexOf(socket.id);
        rooms[socketroom[socket.id]].splice(index, 1);
        io.to(socketroom[socket.id]).emit('user count', rooms[socketroom[socket.id]].length);
        delete socketroom[socket.id];
        console.log('--------------------');
        console.log(rooms[socketroom[socket.id]]);

        //toDo: push socket.id out of rooms
    });
})



//Define the Payment Method 
app.post('/payment', async (req, res) => {
let status, error;
const {token,amount}= req.body;
try {
  
  await Stripe.charges.create({
    source: token.id,
    amount,
    currency:'usd',
  });
  staus= "success";
} catch (error) {
  console.log(error)
  status="Failure";
}
res.json({error, status})
});
// routes for dialogflow instructions
// app.use('/api/dialogflow', require('./routes/dialogflow'));    

// Define API routes
app.use("/", require("./routes"));

//upload 
const mediaRoutes = require("./routes/media");
//chatbot assistant
//Routes
const talkToChatbot = require('./chatbot/chatbot')
const fulfillmentRoutes = require('./chatbot/fulfillment')
let jsonParser = express.json()
let urlEncoded = express.urlencoded({ extended: true })

app.use(morgan('dev'))
//dialogflow assistant route
app.post('/chatbot', jsonParser, urlEncoded, async (req, res) => {
    const message = req.body.message
    //console.log('message' + message)
  
    talkToChatbot(message)
      .then((response) => {
        res.send({ message: response })
      })
      .catch((error) => {
        console.log('Something went wrong: ' + error)
        res.send({
          error: 'Error occured here',
        })
      })
  })
  app.use(fulfillmentRoutes)


app.use("/api/v1/media", mediaRoutes);
app.use("/public", express.static(path.join(__dirname, "public")));

app.use('/api/upload', uploadRoutes);
// Mongodb database connection
mongoose.connect('mongodb+srv://admin:admin@cluster0.n5lxrnq.mongodb.net/', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
.then(() => {
  console.log('Connected to MongoDB Atlas');
  server.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`);
  });
})
.catch(error => {
  console.error('Error connecting to MongoDB Atlas:', error);
});
