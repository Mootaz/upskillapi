const express = require("express");
router = express.Router();
const multer = require("multer");
const mediaController = require("../controllers/course/coursecontroller");
const fs = require("fs");
const path = require("path");
const userController = require("../controllers/user");
const jobOfferController = require("../controllers/jobOffer");
const mentoringRequestsController = require("../controllers/mentoringRequests");
const notificationsController = require("../controllers/notifications");
const messagesController = require("../controllers/messages");
const courseController = require("../controllers/course/coursecontroller");
const UserController2 = require("../controllers/userController2");
const AdminController = require("../controllers/user/admincontroller");

const paymentController = require("../controllers/payment/paymentcontroller");
const likeController = require("../controllers/Like/likeController");
const dislikeController = require("../controllers/Dislike/dislikeController");
const QuizController = require("../controllers/Quiz/quizController");
const openaiController = require('../controllers/openai/openai');







/* ---------------------------------- User ---------------------------------- */

router.get("/user/getUser", userController.getUser);
router.get("/user/getadminuser", AdminController.getAllUser);
router.delete("/user/deleteadminuser/:id", AdminController.deleteAdmin);
router.get("/user/getAllUserstatistics", AdminController.getAllUserstatistics);

router.get("/user/getUser2/:id", UserController2.getUserId);
router.get("/user/getUsersByRole", userController.fetchUsersByRole);
router.get("/user/getUserList", userController.getUserList);
router.post("/user/create", userController.create);
router.post("/user/updateUser", userController.updateUser);
router.post("/user/uploadCV", userController.uploadCV);
router.post("/user/login", userController.authentificate);
router.post("/user/googleLogin", userController.googleLogin);
router.post("/user/forgotPassword", userController.forgotPassword);
router.post("/user/reset-password", userController.resetPassword);
router.post("/user/insertUserCV", userController.insertUserCV);
router.post("/user/updateMentorServices", userController.updateMentorServices);
router.post("/user/upsertUserRole", userController.upsertUserRole);
router.post("/user/uploadIMG", userController.uploadIMG);

/* ----------------------------------- courses----------------------------------- */
router.post("/course/addcourse", courseController.createCourse);
router.get("/course/getallcourse", courseController.getAllCourses);
router.get("/api/media/course/:courseId", courseController.getByCourseIdMedia);
router.get("/course/getcoursebyid/:id", courseController.getCourseById);
router.put("/course/updatecourse/:id", courseController.updateCourse);
router.delete("/course/deletecourse/:id", courseController.deleteCourse);
router.post('/users/:userId/recommendations/:courseId', courseController.addRecommendationToUser);
router.delete('/users/:userId/recommendations/:courseId', courseController.deleteRecommendationFromUser);
router.get('/users/:userId/recommendations', courseController.getUserRecommendations);
router.get('/courses/most-liked', courseController.getMostLikedCourses);
router.get("/courses/getAllcoursestatistics", AdminController.getAllcoursestatistics);
router.post("/chatboat",openaiController.generateAssistantResponse);

/* ----------------------------------- Quizz ----------------------------------- */
router.post("/quiz/addquiz", QuizController.createQuestion);
router.get("/quiz/getallquiz", QuizController.getAllQuestions);
router.get("/quiz/getquizbyid/:id", QuizController.getQuestionById);

/* ----------------------------------- Upload video,file courses----------------------------------- */



/* ----------------------------------- Payment ----------------------------------- */
router.post("/payment/createpayment", paymentController.createPayment);
router.put("/payment/updatestatus/:paymentId", paymentController.updatePaymentStatus);
// Define a route to get payment statistics
router.get('/payment/statistics',paymentController.getPaymentStatistics);

/* ----------------------------------- Like ----------------------------------- */
router.post("/like/addlike", likeController.like );
router.get("/videos/:videoId/likes", likeController.getLikes );


/* ----------------------------------- DisLike ----------------------------------- */
router.post("/dislike", dislikeController.Dislike );


/* ----------------------------------- jobOffer ----------------------------------- */

router.post("/jobOffer/addJobOffer", jobOfferController.addJobOffer);
/* --------------------------- mentoring requests --------------------------- */
router.post(
  "/mentoringRequest/addMentoringRequest",
  mentoringRequestsController.addMentoringRequest
);
router.post(
  "/mentoringRequest/updateMentoringRequestStatus",
  mentoringRequestsController.updateMentoringRequestStatus
);
router.get(
  "/mentoringRequest/getMentoringRequests",
  mentoringRequestsController.getMentoringRequests
);

/* ------------------------------ Notification ------------------------------ */
router.get(
  "/notification/getUserNotifications",
  notificationsController.getUserNotificationsList
);

/* -------------------------------- Messages -------------------------------- */

router.post("/message/sendMessage", messagesController.sendMessage);








module.exports = router;
