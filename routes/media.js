const express = require("express");
const mediaController = require("../controllers/course/coursecontroller");
const multer = require("multer");
const fs = require("fs");
const path = require("path");
const Media = require("../models/course/Media");
const {User} = require("../models/user");
const moment = require('moment'); // Import the moment library or any other library you prefer for date formatting
const { fetchMostPopularComment } = require('../service/Like/likeService');

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    if (!fs.existsSync("public")) {
      fs.mkdirSync("public");
    }

    if (!fs.existsSync("public/videos")) {
      fs.mkdirSync("public/videos");
    }

    cb(null, "public/videos");
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + file.originalname);
  },
});

const upload = multer({
  storage: storage,
  fileFilter: function (req, file, cb) {
    var ext = path.extname(file.originalname);

    if (ext !== ".mkv" && ext !== ".mp4"  && ext !== ".jpeg" && ext !== ".png"  && ext !== ".pdf") {
      return cb(new Error("Only videos are allowed!"));
    }

    cb(null, true);
  },
});

const router = express.Router();



//get all media
router.get("/all", mediaController.getAll);

//getMediaBYId 

// Get media by ID
router.get("/:id", mediaController.getByIdMedia);


//post create new media
router.post(
  "/create",
  upload.fields([
    {
      name: "videos",
      resume:"resume",
     
      maxCount: 1,
    },
  ]),
  mediaController.create
);


router.get('/:id/comments', async (req, res) => {
  try {
    const media = await Media.findById(req.params.id)
      .populate('comments.User_id', 'firstName lastName')
      .exec();

    if (!media) {
      return res.status(404).json({ message: 'Media not found' });
    }

    const comments = media.comments.map(async comment => {
      try {
        console.log(media.comments);
        const commentUser = await User.findById(comment.User_id);
        if (!commentUser) {
          throw new Error('Comment user not found');
        }
        const commentsender = {
          firstName: commentUser.firstName,
          lastName: commentUser.lastName,
          _id: commentUser._id,
        };
       
        return {
          ...comment._doc,
          commentsender,
          User_id: commentUser._id,
        };
      } catch (error) {
        console.log(`Error fetching comment sender: ${error}`);
        return {
          ...comment._doc,
          commentsender: null,
        };
      }
    });

    const resolvedComments = await Promise.all(comments);

    res.status(200).json({ comments: resolvedComments });
  } catch (error) {
    console.log(`Error fetching comments: ${error}`);
    res.status(500).json({ message: 'Failed to fetch comments' });
  }
});



// Create a new comment
router.post('/:id/comments', async (req, res) => {
  try {
    const media = await Media.findById(req.params.id);
    if (!media) {
      return res.status(404).json({ message: 'Media not found' });
    }

    const { text, User_id } = req.body;

    // Find the user associated with the comment
    const commentUser = await User.findById(User_id);
    if (!commentUser) {
      throw new Error('Comment user not found');
    }

    // Create a new comment object with the current date and time
    const newComment = {
      text,
      User_id,
      date: moment().format('YYYY-MM-DD HH:mm:ss'),
      commentsender: {
        firstName: commentUser.firstName,
        lastName: commentUser.lastName,
        _id: commentUser._id,
      },
    };

    media.comments.push(newComment);
    await media.save();

    // Fetch the comment user's information
    const commentsWithUser = await Promise.all(
      media.comments.map(async (comment) => {
        const commentUser = await User.findById(comment.User_id);
        if (!commentUser) {
          throw new Error('Comment user not found');
        }

        return {
          ...comment.toObject(),
          commentsender: {
            firstName: commentUser.firstName,
            lastName: commentUser.lastName,
            _id: commentUser._id,
          },
        };
      })
    );

    res.status(201).json({ comments: commentsWithUser });
  } catch (error) {
    res.status(500).json({ message: 'Failed to add comment' });
  }
});




// Create a new comment reply
router.post('/:id/comments/:commentId/replies', async (req, res) => {
  try {
    const media = await Media.findById(req.params.id);
    if (!media) {
      return res.status(404).json({ message: 'Media not found' });
    }

    const comment = media.comments.find((comment) => comment._id == req.params.commentId);
    if (!comment) {
      return res.status(404).json({ message: 'Comment not found' });
    }

    const { text, User_id } = req.body;

    // Create a new reply object with the current date and time
    const newReply = {
      text,
      User_id,
      date: moment().format('YYYY-MM-DD HH:mm:ss'),
    };

    comment.replies.push(newReply);
    await media.save();

    
    res.status(201).json({comment });
  } catch (error) {
    res.status(500).json({ message: 'Failed to add comment reply' });
  }
});


// Delete a comment reply
router.delete('/:id/comments/:commentId/replies/:replyId', async (req, res) => {
  try {
    const media = await Media.findById(req.params.id);
    if (!media) {
      return res.status(404).json({ message: 'Media not found' });
    }

    const comment = media.comments.find((comment) => comment._id == req.params.commentId);
    if (!comment) {
      return res.status(404).json({ message: 'Comment not found' });
    }

    const reply = comment.replies.find((reply) => reply._id == req.params.replyId);
    if (!reply) {
      return res.status(404).json({ message: 'Reply not found' });
    }

    // Remove the reply from the comment's replies array
    comment.replies = comment.replies.filter((reply) => reply._id != req.params.replyId);

    await media.save();

    res.status(200).json({ message: 'Reply deleted successfully' });
  } catch (error) {
    res.status(500).json({ message: 'Failed to delete reply' });
  }
});





//liking and disliking comment 

router.post('/:mediaId/comments/:commentId/like', async (req, res) => {
  try {
    const { mediaId, commentId } = req.params;
    const { userId } = req.body;

    const media = await Media.findById(mediaId);
    if (!media) {
      return res.status(404).json({ message: 'Media not found' });
    }

    const comment = media.comments.id(commentId);
    if (!comment) {
      return res.status(404).json({ message: 'Comment not found' });
    }

    if (comment.likes.includes(userId)) {
      return res.status(400).json({ message: 'Comment already liked by the user' });
    }

    comment.likes.push(userId);
    await media.save();

    res.json({ message: 'Comment liked successfully' });
  } catch (error) {
    res.status(500).json({ message: 'Failed to like comment' });
  }
});

// Dislike a comment
router.post('/:mediaId/comments/:commentId/dislike', async (req, res) => {
  try {
    const { mediaId, commentId } = req.params;
    const { userId } = req.body;

    const media = await Media.findById(mediaId);
    if (!media) {
      return res.status(404).json({ message: 'Media not found' });
    }

    const comment = media.comments.id(commentId);
    if (!comment) {
      return res.status(404).json({ message: 'Comment not found' });
    }

    if (comment.dislikes.includes(userId)) {
      return res.status(400).json({ message: 'Comment already disliked by the user' });
    }

    comment.dislikes.push(userId);
    await media.save();

    res.json({ message: 'Comment disliked successfully' });
  } catch (error) {
    res.status(500).json({ message: 'Failed to dislike comment' });
  }
});

// Delete a comment
router.delete('/:id/comments/:commentId', async (req, res) => {
  try {
    const media = await Media.findById(req.params.id);
    if (!media) {
      return res.status(404).json({ message: 'Media not found' });
    }

    const comment = media.comments.id(req.params.commentId);
    if (!comment) {
      return res.status(404).json({ message: 'Comment not found' });
    }

    comment.remove();
    await media.save();

    res.status(200).json({ comments:comment});
  } catch (error) {
    res.status(500).json({ message: 'Failed to delete comment' });
  }
});


// Update a comment
router.put('/:id/comments/:commentId', async (req, res) => {
  try {
    const media = await Media.findById(req.params.id);
    if (!media) {
      return res.status(404).json({ message: 'Media not found' });
    }

    const comment = media.comments.id(req.params.commentId);
    if (!comment) {
      return res.status(404).json({ message: 'Comment not found' });
    }

    const { text } = req.body;

    // Update the comment text
    comment.text = text;
    comment.date = moment().format('YYYY-MM-DD HH:mm:ss'); // Update the date and time

    await media.save();

    res.status(200).json({ message: 'Comment updated successfully' });
  } catch (error) {
    res.status(500).json({ message: 'Failed to update comment' });
  }
});


//fetchcomments 

router.get('/videos/:videoId/most-popular-comment', async (req, res) => {
  try {
    const { videoId } = req.params;

    // Call the fetchMostPopularComment function
    const mostPopularComment = await fetchMostPopularComment(videoId);

    res.json({ mostPopularComment });
  } catch (error) {
    console.error('Error fetching most popular comment:', error);
    res.status(500).json({ message: 'Failed to fetch most popular comment' });
  }
});







//Delete new media 
router.delete("/delete/:id", mediaController.delete)




module.exports = router;
