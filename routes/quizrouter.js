// quizController.js

const QuizService = require('../service/Quiz/QuizService');

const router = express.Router();

router.get('/questions', async (req, res) => {
  try {
    const questions = await QuizService.getQuestions();
    res.json(questions);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

router.post('/score', (req, res) => {
  const { questions, answers } = req.body;

  const score = QuizService.calculateScore(questions, answers);
  res.json({ score });
});

module.exports = router;
